package com.example.jamal.fyp;

import android.test.suitebuilder.TestSuiteBuilder;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * Created by jalal on 3/2/2017.
 */

public class FullTestSuit extends TestSuite {
    public static Test suite() {
        return new TestSuiteBuilder(FullTestSuit.class)
                .includeAllPackagesUnderHere().build();
    }

    public FullTestSuit() {
        super();
    }
}
