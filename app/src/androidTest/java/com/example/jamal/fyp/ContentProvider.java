package com.example.jamal.fyp;

import android.content.ComponentName;
import android.content.UriMatcher;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.net.Uri;
import android.test.AndroidTestCase;

import com.example.jamal.fyp.Data.LocationDataContract;
import com.example.jamal.fyp.Data.LocationProvider;

/**
 * Created by jalal on 3/3/2017.
 */

public class ContentProvider extends AndroidTestCase {

    private static final Uri TEST_LOCATION_DIR = LocationDataContract.LocationEntry.CONTENT_URI;

   public void testUriMatcher()
    {
        UriMatcher testMatcher = LocationProvider.buildUriMatcher();
        assertEquals("Error: The LOCATION URI was matched incorrectly.",
                testMatcher.match(TEST_LOCATION_DIR), LocationProvider.LOCATION);
    }

    public void testProviderRegistry() {

        PackageManager pm = mContext.getPackageManager();

// We define the component name based on the package name from the context and the
// WeatherProvider class.
        ComponentName componentName = new ComponentName(mContext.getPackageName(),
                LocationProvider.class.getName());
        try {
// Fetch the provider info using the component name from the PackageManager
// This throws an exception if the provider isn't registered.
            ProviderInfo providerInfo = pm.getProviderInfo(componentName, 0);

// Make sure that the registered authority matches the authority from the Contract.
            assertEquals("Error: WeatherProvider registered with authority: " + providerInfo.authority +
                            " instead of authority: " + LocationDataContract.CONTENT_AUTHORITY,
                    providerInfo.authority, LocationDataContract.CONTENT_AUTHORITY);
        } catch (PackageManager.NameNotFoundException e) {
// I guess the provider isn't registered correctly.
            assertTrue("Error: WeatherProvider not registered at " + mContext.getPackageName(),
                    false);
        }
    }


    public void testGetType() {

        String type = mContext.getContentResolver().getType(LocationDataContract.LocationEntry.CONTENT_URI);
        assertEquals(LocationDataContract.LocationEntry.CONTENT_TYPE, type);
    }


}
