package com.example.jamal.fyp;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.test.AndroidTestCase;

import com.example.jamal.fyp.Data.LocationDataContract;
import com.example.jamal.fyp.Data.LocationDbHelper;

/**
 * Created by jalal on 3/2/2017.
 */

public class testDb extends AndroidTestCase{

      public void testDbDeletion() throws Throwable
    {
        mContext.deleteDatabase(LocationDbHelper.DB_NAME);
        assertTrue(true);
    }
    public void testDbCreation() throws Throwable {
        mContext.deleteDatabase(LocationDbHelper.DB_NAME);
        SQLiteDatabase db = new LocationDbHelper(mContext).getWritableDatabase();
        assertEquals(true, db.isOpen());
        db.close();
    }

    public void testInsert() throws Throwable
    {
        double lat = 1234.3;
        double lng = 123.4;
        String city = "lhr";
        String addr = "abc";
        String country = "pak";
        int time = 12345;
        long rowId;


        LocationDbHelper dbHelper = new LocationDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues ct = new ContentValues();

        ct.put(LocationDataContract.LocationEntry.LATITUDE,lat);
        ct.put(LocationDataContract.LocationEntry.LONGITUDE,lng);
        ct.put(LocationDataContract.LocationEntry.ADDRESS,addr);
        ct.put(LocationDataContract.LocationEntry.CITY,city);
        ct.put(LocationDataContract.LocationEntry.COUNTRY,country);
        ct.put(LocationDataContract.LocationEntry.TIME,time);

        Uri uri = mContext.getContentResolver().insert(LocationDataContract.LocationEntry.CONTENT_URI, ct);
        rowId = ContentUris.parseId(uri);
       // rowId = db.insert(LocationDataContract.LocationEntry.TABLE_NAME,null,ct);
        if(rowId >= 0)
        {
            assertTrue(true);
        }

        String[] Coloumns =   {LocationDataContract.LocationEntry._ID ,
                LocationDataContract.LocationEntry.LATITUDE,
                LocationDataContract.LocationEntry.LONGITUDE,
                LocationDataContract.LocationEntry.ADDRESS,
                LocationDataContract.LocationEntry.CITY,
                LocationDataContract.LocationEntry.COUNTRY,
                LocationDataContract.LocationEntry.TIME};

        Cursor result = mContext.getContentResolver().query(LocationDataContract.LocationEntry.CONTENT_URI, null, null, null, null);
       // Cursor result = db.query(LocationDataContract.LocationEntry.TABLE_NAME,Coloumns,null,null,null,null,null);
        if(result.moveToFirst())
        {
            int lat_index = result.getColumnIndex(LocationDataContract.LocationEntry.LATITUDE);
            Double db_latitude = result.getDouble(lat_index);

            int lng_index = result.getColumnIndex(LocationDataContract.LocationEntry.LONGITUDE);
            double db_longitude = result.getDouble(lng_index);

            int add_index = result.getColumnIndex(LocationDataContract.LocationEntry.ADDRESS);
            String db_address = result.getString(add_index);

            int city_index = result.getColumnIndex(LocationDataContract.LocationEntry.CITY);
            String db_city = result.getString(city_index);

            int country_index = result.getColumnIndex(LocationDataContract.LocationEntry.COUNTRY);
            String db_country = result.getString(country_index);

            int time_index = result.getColumnIndex(LocationDataContract.LocationEntry.TIME);
            int db_time = result.getInt(time_index);

            assertEquals(db_latitude,lat);
            assertEquals(db_longitude,lng);
            assertEquals(db_address,addr);
            assertEquals(db_city,city);
            assertEquals(db_country,country);
            assertEquals(db_time,time);


        }
        else
        {
            fail("No Value Returned");
        }
    }

}
