package com.example.jamal.fyp.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by jalal on 3/26/2017.
 */

public class RescueSyncService extends Service {

    private static final Object sSyncAdapterLock = new Object();
    private  RescueSyncAdapter sRescueSyncAdapter = null;

    @Override
    public void onCreate() {
        Log.d("RescueSyncService", "onCreate - SunshineSyncService");
        synchronized (sSyncAdapterLock) {
            if (sRescueSyncAdapter == null) {
                sRescueSyncAdapter = new RescueSyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return sRescueSyncAdapter.getSyncAdapterBinder();
    }
}
