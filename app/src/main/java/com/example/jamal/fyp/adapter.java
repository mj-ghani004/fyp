package com.example.jamal.fyp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by jalal on 12/9/2016.
 */

public class adapter extends RecyclerView.Adapter<adapter.viewHolder> {

    Context context;
    ArrayList<String> AS;

    public adapter(ArrayList<String> temp , Context ctx) {

        this.context = ctx;
        this.AS = temp;

    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_layout,parent,false);
        viewHolder vh = new viewHolder(view,context);
        return vh;
    }

    @Override
    public void onBindViewHolder(viewHolder holder, int position) {
        holder.textView.setText(AS.get(position));
    }

    @Override
    public int getItemCount() {
        return AS.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder
    {

        TextView textView;
        public viewHolder(View itemView , Context ctx) {

            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.txt_view_location);
        }
    }
}
