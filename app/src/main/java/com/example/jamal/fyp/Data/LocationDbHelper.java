package com.example.jamal.fyp.Data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by jalal on 3/2/2017.
 */

public class LocationDbHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;
    public static final String DB_NAME = "location.db";

    public LocationDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        final String create_table_locationEntry = "CREATE TABLE " + LocationDataContract.LocationEntry.TABLE_NAME +" ("+
                LocationDataContract.LocationEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                LocationDataContract.LocationEntry.LATITUDE + " REAL NOT NULL, "+
                LocationDataContract.LocationEntry.LONGITUDE + " REAL NOT NULL, "+
                LocationDataContract.LocationEntry.ADDRESS + " TEXT NOT NULL, "+
                LocationDataContract.LocationEntry.CITY + " TEXT NOT NULL, "+
                LocationDataContract.LocationEntry.COUNTRY + " TEXT NOT NULL, "+
                LocationDataContract.LocationEntry.TIME + " REAL NOT NULL);";

            sqLiteDatabase.execSQL(create_table_locationEntry);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
