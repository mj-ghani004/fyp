package com.example.jamal.fyp.Data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

/**
 * Created by jalal on 3/3/2017.
 */

public class LocationProvider extends ContentProvider {

    public static final int LOCATION = 300;
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private LocationDbHelper mOpenHelper;


    public static UriMatcher buildUriMatcher()
    {
        final UriMatcher Matcher;
        Matcher = new UriMatcher(UriMatcher.NO_MATCH);
        Matcher.addURI(LocationDataContract.CONTENT_AUTHORITY, LocationDataContract.PATH_LOCATION, LOCATION);
        return Matcher;
    }
    @Override
    public boolean onCreate() {
        mOpenHelper = new LocationDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Cursor retCursor ;
        switch (sUriMatcher.match(uri))
        {
            case LOCATION:{

                retCursor = mOpenHelper.getReadableDatabase().query(
                        LocationDataContract.LocationEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,null,null,sortOrder
                );
                break;
            }

            default:
                throw  new UnsupportedOperationException("Unknown Uri" + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(),uri);
        return retCursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {

        final int result = sUriMatcher.match(uri);
        if(result == LOCATION)
        {
            return LocationDataContract.LocationEntry.CONTENT_TYPE;
        }
        else {

                throw new UnsupportedOperationException("Unknown Uri" + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri = null;
            long _id;
        switch(match)
        {
            case LOCATION:
            {
                _id = db.insert(LocationDataContract.LocationEntry.TABLE_NAME,null,contentValues);
                if(_id > 0)
                {
                    returnUri = LocationDataContract.LocationEntry.buildLocationUri(_id);
                }
                else
                {
                    throw new SQLException("Failed to Insert Row" + uri);
                }
                break;
            }

            default:

                throw new UnsupportedOperationException("Unknown Uri" + uri);
        }
        getContext().getContentResolver().notifyChange(uri,null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }
}
