package com.example.jamal.fyp.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.example.jamal.fyp.Data.FullAddress;
import com.example.jamal.fyp.Data.LocationDataContract;
import com.example.jamal.fyp.Data.Utility;
import com.example.jamal.fyp.R;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by jalal on 3/26/2017.
 */

public class RescueSyncAdapter extends AbstractThreadedSyncAdapter {

    public final String LOG_TAG = RescueSyncAdapter.class.getSimpleName();
    private final Context mContext;
    public static final int SYNC_INTERVAL = 30;
    public static final int SYNC_FLEXTIME = SYNC_INTERVAL/3;

    //GeoCoder
    Geocoder geocoder;
    List<Address> addresses;

    //Class Full Address
    FullAddress fullAddress;

    //Class Utility
    static Utility utility;



    public RescueSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContext = context;
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Log.d(LOG_TAG, "onPerformSync Called.");
        //GeoCoder
        Intent intent = new Intent();
        intent.setAction("com.example.CUSTOM_INTENT");
        mContext.sendBroadcast(intent);

        geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            Double Lat = utility.getLatitute();
            Double lng = utility.getLongitute();


            addresses = geocoder.getFromLocation(utility.getLatitute(), Utility.getLongitute(), 1);

            String address = addresses.get(0).getAddressLine(0);
            String city = addresses.get(0).getLocality();
            String country = addresses.get(0).getCountryName();
            fullAddress = new FullAddress(address,city,country);

            ContentValues ct = new ContentValues();

            ct.put(LocationDataContract.LocationEntry.LATITUDE,utility.getLatitute());
            ct.put(LocationDataContract.LocationEntry.LONGITUDE,utility.getLongitute());
            ct.put(LocationDataContract.LocationEntry.ADDRESS,address);
            ct.put(LocationDataContract.LocationEntry.CITY,city);
            ct.put(LocationDataContract.LocationEntry.COUNTRY,country);
            ct.put(LocationDataContract.LocationEntry.TIME,System.currentTimeMillis());

            Uri uri = mContext.getContentResolver().insert(LocationDataContract.LocationEntry.CONTENT_URI, ct);

        }catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * Helper method to have the sync adapter sync immediately
     * @param context The context used to access the account service
     */
    public static void syncImmediately(Context context) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(getSyncAccount(context),
                context.getString(R.string.content_authority), bundle);
    }

    /**
     * Helper method to get the fake account to be used with SyncAdapter, or make a new one
     * if the fake account doesn't exist yet.  If we make a new account, we call the
     * onAccountCreated method so we can initialize things.
     *
     * @param context The context used to access the account service
     * @return a fake account.
     */
    public static Account getSyncAccount(Context context) {
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        // Create the account type and default account
        Account newAccount = new Account(
                context.getString(R.string.app_name), context.getString(R.string.sync_account_type));

        // If the password doesn't exist, the account doesn't exist
        if ( null == accountManager.getPassword(newAccount) ) {

        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                return null;
            }
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call ContentResolver.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
            onAccountCreated(newAccount , context);
        }

        return newAccount;
    }

    /**
     * Helper method to schedule the sync adapter periodic execution
     */
    public static void configurePeriodicSync(Context context, int syncInterval, int flexTime) {
        Account account = getSyncAccount(context);
        String authority = context.getString(R.string.content_authority);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // we can enable inexact timers in our periodic sync
            SyncRequest request = new SyncRequest.Builder().
                    syncPeriodic(syncInterval, flexTime).
                    setSyncAdapter(account, authority).
                    setExtras(new Bundle()).build();
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account,
                    authority, new Bundle(),syncInterval);
        }
    }


    private static void onAccountCreated(Account newAccount, Context context) {
        /*
         * Since we've created an account
         */
        RescueSyncAdapter.configurePeriodicSync(context, SYNC_INTERVAL, SYNC_FLEXTIME);

        /*
         * Without calling setSyncAutomatically, our periodic sync will not be enabled.
         */
        ContentResolver.setSyncAutomatically(newAccount, context.getString(R.string.content_authority), true);


        /*
         * Finally, let's do a sync to get things started
         */
        syncImmediately(context);
    }

    public static void initializeSyncAdapter(Context context) {
        getSyncAccount(context);
    }



}
