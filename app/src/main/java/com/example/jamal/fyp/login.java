package com.example.jamal.fyp;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class login extends AppCompatActivity implements View.OnClickListener {

    Button btn_login ;
    ViewGroup mRoot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT>=21)
        {
            TransitionInflater transitionInflater = TransitionInflater.from(this);
            Transition transition = transitionInflater.inflateTransition(R.transition.transitions);
            getWindow().setExitTransition(transition);
        }

        setContentView(R.layout.login);
        btn_login = (Button) findViewById(R.id.submit);
        btn_login.setOnClickListener(this);
        mRoot = (ViewGroup) findViewById(R.id.activity_main3);


    }

    @Override
    public void onClick(View view) {

        ActivityOptionsCompat option = ActivityOptionsCompat.makeSceneTransitionAnimation(this,null);
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent,option.toBundle());
    }
}
