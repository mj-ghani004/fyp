package com.example.jamal.fyp.Data;

/**
 * Created by jalal on 2/27/2017.
 */

public class FullAddress {

    String City;
    String Country;
    String Address;
    public FullAddress(String address , String city , String country)
    {
        this.Address = address;
        this.City = city;
        this.Country = country;
    }

    public String getCompleteAddress()
    {
        return Address+" ,"+City+" ,"+Country;
    }
}
