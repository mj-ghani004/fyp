package com.example.jamal.fyp.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by jalal on 3/26/2017.
 */

public class RescueAuthenticatorService extends Service {

    // Instance field that stores the authenticator object
    private RescueAuthenticator mAuthenticator;

    @Override
    public void onCreate() {
        // Create a new authenticator object
        mAuthenticator = new RescueAuthenticator(this);
    }

    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}
