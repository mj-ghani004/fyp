package com.example.jamal.fyp;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.jamal.fyp.Data.FullAddress;
import com.example.jamal.fyp.Data.Utility;

/**
 * Created by jalal on 3/3/2017.
 */

public class LocationAdapter extends CursorAdapter {
    Utility utility ;

    public LocationAdapter(Context context, Cursor c) {
        super(context, c);
        utility = new Utility(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {

        View view = LayoutInflater.from(context).inflate(R.layout.card_view_layout,viewGroup,false);

        viewHolder viewHolder = new viewHolder(view);
        //Setting up TAG. TAG can be used to store any object and afterwards access it within view
        view.setTag(viewHolder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        viewHolder viewHolder = (viewHolder)view.getTag();

        String address = cursor.getString(MainActivity.COL_ADDRESS);
        String city = cursor.getString(MainActivity.COL_CITY);
        String country = cursor.getString(MainActivity.COL_COUNTRY);
        Long time = cursor.getLong(MainActivity.COL_TIME);

        FullAddress fullAddress = new FullAddress(address,city,country);

        viewHolder.txt_location.setText(fullAddress.getCompleteAddress());
        viewHolder.txt_location_time.setText(utility.getLocationTime(time));

    }

    public static class viewHolder
    {
        TextView txt_location ;
        TextView txt_location_time ;


        public  viewHolder(View view)
        {
            txt_location =     (TextView) view.findViewById(R.id.txt_view_location);
            txt_location_time = (TextView) view.findViewById(R.id.txt_view_location_time);

        }
    }
}
