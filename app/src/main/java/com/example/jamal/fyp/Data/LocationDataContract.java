package com.example.jamal.fyp.Data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by jalal on 3/2/2017.
 */

public class LocationDataContract {

    public static final String CONTENT_AUTHORITY = "com.example.jamal.fyp";
    public static final Uri BASE_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_LOCATION = "location";

public  static class LocationEntry implements BaseColumns{


    public static final Uri CONTENT_URI = BASE_URI.buildUpon().appendPath(PATH_LOCATION).build();
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LOCATION;
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LOCATION;

    public static final String TABLE_NAME = "location";

    public static final String LATITUDE = "latitude";

    public static final String LONGITUDE = "longitude";

    public static final String ADDRESS = "address";

    public static final String CITY = "city";

    public static final String COUNTRY = "country";

    public static final String TIME = "time";


    public static Uri buildLocationUri(long id)
    {
        return ContentUris.withAppendedId(CONTENT_URI, id);
    }

}


}
