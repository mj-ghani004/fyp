package com.example.jamal.fyp;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.widget.TextView;

import com.example.jamal.fyp.Data.FullAddress;
import com.example.jamal.fyp.Data.Utility;

import java.util.List;

/**
 * Created by jalal on 3/3/2017.
 */

public  class LocationTask extends AsyncTask<Void,Void,Void> {

    private final String LOG_TAG = LocationTask.class.getSimpleName();
    private final Context mContext;
    private TextView CurrentLocation;
    private TextView CurrentLocationTime;

    //GeoCoder
    Geocoder geocoder;
    List<Address> addresses;

    //Class Full Address
   private FullAddress fullAddress;

    //Class Utility
   private Utility utility;
    long rowId;


    public LocationTask(Context context, TextView current , TextView time) {
        mContext = context;
        CurrentLocation = current;
        CurrentLocationTime = time;
    }
    @Override
    protected Void doInBackground(Void... voids) {

        //GeoCoder
/*
        geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(utility.getLatitute(), Utility.getLongitute(), 1);

            String address = addresses.get(0).getAddressLine(0);
            String city = addresses.get(0).getLocality();
            String country = addresses.get(0).getCountryName();
            fullAddress = new FullAddress(address,city,country);

            ContentValues ct = new ContentValues();

                ct.put(LocationDataContract.LocationEntry.LATITUDE,utility.getLatitute());
                ct.put(LocationDataContract.LocationEntry.LONGITUDE,utility.getLongitute());
                ct.put(LocationDataContract.LocationEntry.ADDRESS,address);
                ct.put(LocationDataContract.LocationEntry.CITY,city);
                ct.put(LocationDataContract.LocationEntry.COUNTRY,country);
                ct.put(LocationDataContract.LocationEntry.TIME,System.currentTimeMillis());

                Uri uri = mContext.getContentResolver().insert(LocationDataContract.LocationEntry.CONTENT_URI, ct);
                rowId = ContentUris.parseId(uri);

        }catch (IOException e) {
            e.printStackTrace();
        }
*/

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      /*  CurrentLocation.setText(fullAddress.getCompleteAddress());
        CurrentLocationTime.setText(utility.getTime(mContext));
        Toast.makeText(mContext,fullAddress.getCompleteAddress() ,Toast.LENGTH_LONG).show();

        if(rowId >= 0)
        {
            Toast.makeText(mContext,"Record Added" ,Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(mContext,"Record Failed to Add" ,Toast.LENGTH_LONG).show();
        }
*/
        super.onPostExecute(aVoid);
    }
}
