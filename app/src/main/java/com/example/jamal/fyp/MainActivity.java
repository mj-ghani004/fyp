package com.example.jamal.fyp;

import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jamal.fyp.Data.LocationDataContract;
import com.example.jamal.fyp.Data.Utility;
import com.example.jamal.fyp.sync.RescueSyncAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, View.OnClickListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int LOCATION_LOADER = 0;
    private static Context mContext;

    //Location Variables
    private static GoogleApiClient mApiClient;

    boolean mapReady = false;
    MapFragment mapFragment;
    MarkerOptions locationMarker;

    //Reference of Views
    TextView CurrentLocation;
    TextView CurrentLocationTime;

    //ListView Varialbes
    ListView listView;
    ImageView mark_as_save;
    LocationAdapter locationAdapter;

    //Class Utility
    private static Utility utility;
    static RescueSyncAdapter rescueSyncAdapter;
    /*===============================================================
                        Activity LifeCycle Methods
     ===============================================================*/
    String[] Coloumns = {LocationDataContract.LocationEntry._ID,
            LocationDataContract.LocationEntry.LATITUDE,
            LocationDataContract.LocationEntry.LONGITUDE,
            LocationDataContract.LocationEntry.ADDRESS,
            LocationDataContract.LocationEntry.CITY,
            LocationDataContract.LocationEntry.COUNTRY,
            LocationDataContract.LocationEntry.TIME};
    /*===============================================================
                        Indices
     ===============================================================*/
    public static final int COL_LOCATION_ID = 0;
    public static final int COL_LATITUDE = 1;
    public static final int COL_LONGITUDE = 2;
    public static final int COL_ADDRESS = 3;
    public static final int COL_CITY = 4;
    public static final int COL_COUNTRY = 5;
    public static final int COL_TIME = 6;

    /*===============================================================
                        Activity LifeCycle Methods
     ===============================================================*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
        if (Build.VERSION.SDK_INT >= 21) {
            Slide slide = new Slide(Gravity.RIGHT);
            slide.setInterpolator(AnimationUtils.loadInterpolator(this, android.R.interpolator.linear_out_slow_in));
            slide.setDuration(1000);

            TransitionInflater transitionInflater = TransitionInflater.from(this);
            Transition transition = transitionInflater.inflateTransition(R.transition.transitions);

            getWindow().setExitTransition(transition);
            getWindow().setEnterTransition(slide);

        }
        //Getting Views
        setContentView(R.layout.activity_main);

        //Adding Toolbar
        ((CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar)).setTitle("Rescue");

        utility = new Utility(this);
        mark_as_save = (ImageView) findViewById(R.id.mark_as_save_button);
        mark_as_save.setOnClickListener(this);
        listView = (ListView) findViewById(R.id.recyclerView);
        listView.setTransitionGroup(true);
        //Location Initialization
        buildApiClient();
        //Maps Fragment Initialization
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        CurrentLocation = (TextView) findViewById(R.id.recent_location);
        mark_as_save = (ImageView) findViewById(R.id.mark_as_save_button);
        CurrentLocationTime = (TextView) findViewById(R.id.current_location_time);
        locationAdapter = new LocationAdapter(this, null);
        RescueSyncAdapter.initializeSyncAdapter(this);


    }

    @Override
    protected void onStart() {
        super.onStart();
        mApiClient.connect();

    }

    @Override
    protected void onStop() {
        if (mApiClient.isConnected()) {
            mApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportLoaderManager().initLoader(LOCATION_LOADER, null, this);

        listView.setAdapter(locationAdapter);
        locationAdapter.notifyDataSetChanged();


    }

/*===================================================================
                    OnClick()
====================================================================*/
    @Override
    public void onClick(View view) {

        if (view == mark_as_save) {
            Bundle bundle = ActivityOptions.makeSceneTransitionAnimation(
                    this, mark_as_save, mark_as_save.getTransitionName()).toBundle();
            Intent save_intent = new Intent(this, markAsSave.class);
            startActivity(save_intent, bundle);
        }
    }

    /*===================================================================
                        Map CallBacks
    ====================================================================*/
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapReady = true;
        GoogleMap m_Map = googleMap;
        LatLng Pucit = new LatLng(utility.getLatitute(), utility.getLongitute());
        CameraPosition position = CameraPosition.builder().target(Pucit).zoom(15).tilt(0).build();
        m_Map.moveCamera(CameraUpdateFactory.newCameraPosition(position));
        locationMarker = new MarkerOptions().position(new LatLng(utility.getLatitute(), utility.getLongitute())).title("Current Location");
        m_Map.addMarker(locationMarker);
        rescueSyncAdapter.syncImmediately(getApplicationContext());
    }

    /*======================================================================
                            Current Location Callbacks
     ======================================================================*/
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            Toast.makeText(this, "Permission Rejected", Toast.LENGTH_LONG).show();
            return;
        }

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mApiClient);
        if (mLastLocation != null) {

            utility.setLatitute(mLastLocation.getLatitude());
            utility.setLongitute(mLastLocation.getLongitude());
            mapFragment.getMapAsync(this);
            // getAddress();
        } else {
            Toast.makeText(this, "Location Null", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    public void buildApiClient() {
        mApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }


    /*============================================================
                    Loader CallBacks
     =============================================================*/
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String sortOrder = LocationDataContract.LocationEntry._ID + " DESC";
        Uri LocationUri = LocationDataContract.LocationEntry.CONTENT_URI;
        return new CursorLoader(this, LocationUri, Coloumns, null, null, sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        locationAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        locationAdapter.swapCursor(null);
    }



    public static class Receiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Toast.makeText(mContext,"Google",Toast.LENGTH_SHORT).show();
            Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mApiClient);
            utility.setLatitute(mLastLocation.getLatitude());
            utility.setLongitute(mLastLocation.getLongitude());

        }
    }


}
