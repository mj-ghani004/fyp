package com.example.jamal.fyp.Data;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.text.format.Time;

import java.text.SimpleDateFormat;
import java.util.List;



/**
 * Created by jalal on 2/28/2017.
 */

public class Utility {

    public static final String DATE_FORMAT = "yyyyMMdd";
    private static Double longitute;
    private static Double latitute;
    private Context context;

    //GeoCoder
    Geocoder geocoder;
    List<Address> addresses;


    public Utility(Context ctx)
    {
        context = ctx;
    }
    public static Double getLatitute() {
        return latitute;
    }

    public static void setLatitute(Double latitute) {
        Utility.latitute = latitute;
    }

    public static Double getLongitute() {
        return longitute;
    }

    public static void setLongitute(Double longitute) {
        Utility.longitute = longitute;
    }

    public static String getTime(Context ctx)
    {

        Time time = new Time();
        time.setToNow();
        long currentTime = System.currentTimeMillis();
        int currentJulianDay = Time.getJulianDay(currentTime, time.gmtoff);

        // Format 12:08:56:AM
        SimpleDateFormat shortenedDateFormat = new SimpleDateFormat("h:mm:ss aaa");
        return shortenedDateFormat.format(currentTime);
    }

    public static String getLocationTime(long timeInMillis)
    {

        Time time = new Time();
        time.setToNow();
        long currentTime = timeInMillis;
        int currentJulianDay = Time.getJulianDay(currentTime, time.gmtoff);

        // Format 12:08:56:AM
        SimpleDateFormat shortenedDateFormat = new SimpleDateFormat("h:mm:ss aaa");
        return shortenedDateFormat.format(currentTime);
    }

/*    //Get Address
    public FullAddress getAddress(double latitute , double longitute)
    {
        //GeoCoder
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(this.latitute, this.longitute, 1);

            String address = addresses.get(0).getAddressLine(0);
            String city = addresses.get(0).getLocality();
            String country = addresses.get(0).getCountryName();
            fullAddress = new FullAddress(address,city,country);

            //Updating the UI
            UpdateUI(fullAddress.getCompleteAddress());
            //Toast.makeText(this,fullAddress.getCompleteAddress(), Toast.LENGTH_LONG).show();

        }catch (IOException e) {
            e.printStackTrace();
        }
        return fullAddress;
    }

    public void UpdateUI(String location)
    {
        TextView CurrentLocation= (TextView) ((Activity)context).findViewById(R.id.recent_location);
        CurrentLocation.setText(location);
        for(String x : dummy_list)
        {
            arrayList.add(x);
        }
        list_adapter = new adapter(arrayList,context);
        recyclerView.setAdapter(list_adapter);
        list_adapter.notifyDataSetChanged();
    }*/

}
